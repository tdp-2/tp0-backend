const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');
const hospitalRouter = require('./middlewares/routers/hospital_router.js');
const errorMiddleware = require('./middlewares/main_error_handler.js');

const app = express();
const HOST = process.env.HOST || '0.0.0.0';
const PORT = process.env.PORT || 8080;

app.use(bodyParser.json({ limit: '20mb' }));

// Enable CORS for all routes
app.use(cors());

// Add routers to the API
hospitalRouter(app);

// Add basic error middleware to handle all errors
errorMiddleware(app);

// Start the app in the designated port and host

if (! module.parent) {
  app.listen(PORT, HOST, () => {
    console.log(`Server running on http://${HOST}:${PORT}`);
  });
}

module.exports = app;

