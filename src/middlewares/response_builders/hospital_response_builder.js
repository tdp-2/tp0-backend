function HospitalResponseBuilder() {
  this.buildResponse = function(req, res) {
    const response = {};
    response.hospitals = res.hospitals;
    console.log('Response: %j', response);
    res.status(200).json(response);
  };
}

module.exports = HospitalResponseBuilder;
