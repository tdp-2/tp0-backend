const HospitalController = require('../controllers/hospital_controller.js');
const HospitalResponseBuilder = require('../response_builders/hospital_response_builder.js');

function HospitalRouter(app) {
  const hospitalController = new HospitalController();
  const hospitalResponseBuilder = new HospitalResponseBuilder();

  app.get('/api/hospitals/all',
      hospitalController.getAllHospitals,
      hospitalResponseBuilder.buildResponse,
  );
}

module.exports = HospitalRouter;
