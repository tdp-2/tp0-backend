const fs = require('fs');
const csv = require('csv-parser');

function HospitalController() {
  this.getAllHospitals = async (req, res, next) => {
    const hospitals = [];
    fs.createReadStream('./data/hospitales.csv')
        .pipe(csv())
        .on('data', (data) => hospitals.push(data))
        .on('end', () => {
          res.hospitals = hospitals;
          return next();
        });
  };
}

module.exports = HospitalController;
